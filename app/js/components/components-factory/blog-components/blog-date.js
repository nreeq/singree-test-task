import React, {Component} from 'react'
import PropTypes from 'prop-types'

import {getCurrentTime} from './../../../helpers/time-parser'

class BlogDate extends Component{

    render = () => {

        const currentDate = getCurrentTime(this.props.pubDate, 'day-number')
        const currentDateName = getCurrentTime(this.props.pubDate, 'day-name')
        const currentMonthName = getCurrentTime(this.props.pubDate, 'month-name')
        const currentYear = getCurrentTime(this.props.pubDate, 'year')

        return (
            <div className='blog-date'>
                <div className='blog-item-pub-date'>
                    <p>{currentDate || 0}</p>
                    <div className='blog-line'></div>
                </div>
                <div className='vertical-date'>
                    <p>{currentDateName || 'fix your date'}</p>
                    <p>{currentMonthName || 'please'}, {currentYear || '2000'}</p>
                </div>
            </div>
        )
    }
}

BlogDate.propTypes = {
    pubDate: PropTypes.string
}

export default BlogDate