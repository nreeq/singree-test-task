import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Editor } from '@tinymce/tinymce-react'
import {Link} from 'react-router-dom'

import requests from './../../../constants/requests'

class ArticleCreator extends Component{
    constructor(props){
        super(props)
        this.state = {
            title: '',
            body: ''
        }
    }

    handleType = ev => this.setState({ title: ev.target.value })

    handleEditorChange = ev => this.setState({ body: ev.target.getContent() })

    saveArticle = () => {
        const newArticle = {
            title: this.state.title,
            body: this.state.body
        }
        this.props.asyncRequestHelper(requests.addArticle.actions, requests.addArticle.config, newArticle)
        this.setState({
            title: '',
            body: ''
        })
    }

    render = () => {
        return (
            <div className='editor-content create'>
                <div className='editor-nav'>
                    <button 
                        disabled={this.state.title && this.state.body ? false : true}
                        onClick={this.saveArticle}
                    >create
                    </button>
                    <Link to='/'>cancel / back</Link>
                </div>
                <input
                    value={this.state.title}
                    type='text'
                    onChange={this.handleType}
                    placeholder='Title'
                    required
                />
                <div className='tiny-editor'>
                <Editor
                        value={this.state.body}
                        init={{
                            plugins: 'link image code',
                            toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code | fontsizeselect',
                            fontsize_formats: '8px 10px 12px 14px 18px 24px'
                        }}
                        onChange={this.handleEditorChange}
                    />
                </div>
            </div>
        )
    }
}

ArticleCreator.propTypes = {
    asyncRequestHelper: PropTypes.func
}

export default ArticleCreator