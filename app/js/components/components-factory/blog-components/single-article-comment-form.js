import React, {Component} from 'react'
import PropTypes from 'prop-types'

class CommentForm extends Component{
    sendComment = ev => {
        ev.preventDefault()
        this.props.submitComment()
    }

    render = () => {
        return (
            <form className='comment-form' onSubmit={this.sendComment}>
                <div className='head'>
                    <input
                        maxLength='24'
                        required
                        onChange={ev => this.props.handleTyping(ev.target.name, ev.target.value)}
                        type='text' 
                        name='author'
                        placeholder='name'
                        value={this.props.author}
                    />
                    <input 
                        maxLength='24'
                        onChange={ev => this.props.handleTyping(ev.target.name, ev.target.value)}
                        type='text' 
                        name='email' 
                        placeholder='e-mail'
                        value={this.props.email}
                    />
                </div>
                <textarea 
                    required
                    maxLength='400'
                    onChange={ev => this.props.handleTyping(ev.target.name, ev.target.value)}
                    name='comment'
                    value={this.props.comment}
                />
                <button type='submit'>submit comment</button>
            </form>
        )
    }
}

CommentForm.propTypes = {
    author: PropTypes.string,
    email: PropTypes.string,
    comment: PropTypes.string,
    handleTyping: PropTypes.func,
    submitComment: PropTypes.func
}

export default CommentForm