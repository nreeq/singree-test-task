import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

import BlogDate from './blog-date'
import CommentsField from './single-article-comments'
import ShareSection from './single-article-share'
import ArticleStats from './single-article-stats'

class SingleArticle extends Component{

    render = () => {
        const { singleBlogData } = this.props

        if(singleBlogData._id){
            return (
                <div className='content single-article'>
                    <BlogDate pubDate={singleBlogData.created} />
                    <div className='single-article-image'>
                        <img src={singleBlogData.image} alt={singleBlogData.title}/>
                    </div>
                    <ArticleStats
                        tags={singleBlogData.tags}
                        commentsQuantity={singleBlogData.comments.length}
                        views={singleBlogData.views}
                    />
                    <Link className='single-article-to-edit' to={`/blog/${singleBlogData._id}/edit`}>edit</Link>
                    <h1 className='single-article-title'>{singleBlogData.title}</h1>
                    <div
                        className='single-article-body' 
                        dangerouslySetInnerHTML={{__html: singleBlogData.body}}
                    >
                    </div>
                    <ShareSection />
                    <CommentsField
                        comments={singleBlogData.comments}
                        id={singleBlogData._id}
                        asyncRequestHelper={this.props.asyncRequestHelper}
                    />
                </div>
            )
        } else {
            return <h1>loading...</h1>
        }
    }
}

SingleArticle.propTypes = {
    singleBlogData: PropTypes.object,
    asyncRequestHelper: PropTypes.func
}

export default SingleArticle