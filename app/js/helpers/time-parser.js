const getCurrentTime = (date, preference) => {

    const day = Number(date.slice(0, date.indexOf('/')))
    const month = Number(date.slice(date.indexOf('/') + 1, date.lastIndexOf('/')))
    const year = Number(date.slice(date.lastIndexOf('/') + 1, date.lastIndexOf('/') + 5))

    const timeObj = new Date(year, month, day)

    switch (preference){
        case 'day-number':
            return timeObj.getDate()
        case 'day-name':
            const daysName = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
            return daysName[timeObj.getDay()]
        case 'month-name':
            const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
            return months[timeObj.getMonth() - 1]
        case 'year':
            return timeObj.getFullYear()
        default:
            return false
    }
}

export {getCurrentTime}