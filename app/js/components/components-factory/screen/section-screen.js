import React from 'react'
import PropTypes from 'prop-types'

const SectionScreen = ({...props}) => (
    <div className='section-screen'>
        <h1>{props.section}</h1>
    </div>
)

SectionScreen.propTypes = {
    section: PropTypes.string
}

export default SectionScreen