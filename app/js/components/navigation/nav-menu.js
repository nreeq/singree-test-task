import React, {Component} from 'react'
import NavItem from './nav-item'
import {Link} from 'react-router-dom'
import nav from './../../constants/nav'

class Navigation extends Component{

    constructor(props){
        super(props)
        this.state = {
            isActiveBurger: false
        }
    }

    mapNav = list => {
        return list.map( item => {
            return <NavItem 
                        key={item.id} 
                        link={item.link} 
                        value={item.value}
                    />
        })
    }

    openBurger = () => {
        this.setState({isActiveBurger: !this.state.isActiveBurger})
    }

    render = () => {
        return (
            <div>
                <svg
                    onClick={this.openBurger} 
                    className={`nav-burger${this.state.isActiveBurger?' active-burger':''}`}
                >
                    <line x1='10' y1='15' x2='60' y2='15'></line>
                    <line x1='20' y1='35' x2='50' y2='35'></line>
                    <line x1='20' y1='35' x2='50' y2='35'></line>
                    <line x1='10' y1='55' x2='60' y2='55'></line>
                </svg>
                <div className={`navigation${this.state.isActiveBurger ?' active': ''}`}>
                    <Link className='nav-logo' to='/'>
                        <img src='../../img/logo.png' />
                    </Link>
                    <div className='nav-list'>
                        {this.mapNav(nav)}
                    </div>
                </div>
            </div>
        )
    }
}

export default Navigation