var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	devtool: 'eval-source-map',
	entry: __dirname + "/app/js/index.js",
	output: {
		path: '/',
		publicPath: '/'
	},
	module: {
		loaders: [
			{ test: /\.js?$/, exclude: /node_modules/, loader: 'babel-loader' }, {
				test: /\.scss$/,
				use: [{
					loader: "style-loader" // creates style nodes from JS strings
				}, {
					loader: "css-loader" // translates CSS into CommonJS
				}, {
					loader: "sass-loader" // compiles Sass to CSS
				}]
			},
			{ test: /\.(jpe?g|png|gif|svg)$/i, loaders: 'file-loader' },
			{ test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/, loader: 'file-loader' }
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			chunksSortMode: 'dependency',
			template: __dirname + "/app/index.html"
		}),
		new webpack.HotModuleReplacementPlugin()
	],
	devServer: {
		contentBase: path.join(__dirname, "app"),
		inline: true,
		hot: true,
		compress: true,
		port: 9000,
		historyApiFallback: true,
		publicPath: '/',
	}
}
