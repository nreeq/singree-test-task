import {combineReducers} from 'redux'

import { blogReducer } from './reducer-blog';

const mainReducer = combineReducers({
    blogData: blogReducer
})

export default mainReducer