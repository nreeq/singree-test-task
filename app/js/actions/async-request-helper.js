const fetchPostsRequest = type => {
    return { type }
}
  
const fetchPostsSuccess = (payload, type) => {
    return { type, payload }
}
  
const fetchPostsError = type => {
    return { type }
}

const fetchPosts = (config, data, id) => {
    const URL = `${config.route}${id ? id : ''}`;
    return fetch(URL, { 
        method: config.method,
        headers: config.headers,
        body: data && JSON.stringify(data) || undefined
    })
       .then( response => Promise.all([response, response.json()]));
}

const asyncRequestHelper = (actions, config, data, id) => {
	return (dispatch) => {
        dispatch(fetchPostsRequest(actions.request));
        return fetchPosts(config, data, id).then(([response, json]) =>{
            if(response.status === 200){
                dispatch(fetchPostsSuccess(json, actions.success))
            }
            else{
                dispatch(fetchPostsError(actions.error))
            }
        })
    }
}

export {asyncRequestHelper}