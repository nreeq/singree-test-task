import React, {Component} from 'react'

import ArticleEditor from './../components-factory/blog-components/article-editor'
import SectionScreen from './../components-factory/screen/section-screen'

import requests from './../../constants/requests'

class ArticleEdit extends Component{

    componentDidMount = () => {
        if(!this.props.singleBlogData._id) {
            this.props.asyncRequestHelper(requests.getBlogData.actions, requests.getBlogData.config)
        }
    }

    render = () => {
        return (
            <div>
                <SectionScreen section='edit' />
                <div className='section article-editor'>
                    <ArticleEditor
                        asyncRequestHelper={this.props.asyncRequestHelper}
                        singleBlogData={this.props.singleBlogData}
                    />
                </div>
            </div>
        )
    }
}

export default ArticleEdit