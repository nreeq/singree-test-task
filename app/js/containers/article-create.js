import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'

import ArticleCreate from './../components/pages/article-create'
import {asyncRequestHelper} from './../actions/async-request-helper'

const mapDispatchToProps = dispatch => {
    return bindActionCreators (
        { asyncRequestHelper },
        dispatch
    )
}

export default connect(null, mapDispatchToProps)(ArticleCreate);