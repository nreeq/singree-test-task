import React, {Component} from 'react'
import PropTypes from 'prop-types'
import FontAwesome from 'react-fontawesome'

class ArticleStats extends Component{
    mapTags = (tags) => {
        const items = tags || ['tag', 'tag', 'tag', 'tag']
        return items.map((tag, i) => {
            return <li key={i}>{tag}</li>
        })
    }

    render = () => {

        const views = this.props.views ? this.props.views : Math.round(Math.random() * 1000)

        return (
            <div className='single-article-stats'>
                <ul className='single-article-tags'>
                    <FontAwesome name='tags' />
                    {this.mapTags(this.props.tags)}
                </ul>
                <p className='single-article-comments'>
                    <FontAwesome name='comment' />
                    <span>({this.props.commentsQuantity})</span>
                </p>
                <p className='single-article-views'>
                    <FontAwesome name='eye' />
                    <span>({views})</span>
                </p>
            </div>
        )
    }
}

ArticleStats.propTypes = {
    tags: PropTypes.array,
    commentsQuantity: PropTypes.number,
    views: PropTypes.number
}

export default ArticleStats