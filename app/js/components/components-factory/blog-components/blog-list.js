import React, {Component} from 'react'
import PropTypes from 'prop-types'
import ReactPaginate from 'react-paginate'

import BlogItem from './blog-item'
import {Link} from 'react-router-dom'

class BlogList extends Component{

    constructor(props){
        super(props)
        this.state = {
            page: 1
        }
    }

    renderArticles = (currentData, page) => {
        const quantity = page * 3

        return currentData.map((item, i) => {
            if(i + 1 >= quantity - 2 && i + 1 <= quantity){
            return <BlogItem
                        key={i}
                        pubDate={item.created}
                        link={item._id}
                        title={item.title}
                        image={item.image}
                        description={item.metaDescription}
                    />
            }
        })
    }

    handlePageClick = data => {
        this.setState({
            page: data.selected + 1
        })
    }

    render = () => {
        return (
            <div className='content blog-list'>
                <Link 
                    className='create-new-article' 
                    to='/blog/create'
                >
                    create new article
                </Link>
                {this.renderArticles(this.props.blogData, this.state.page)}
                <ReactPaginate 
                    previousLabel='&#8249;'
                    nextLabel='&#8250;'
                    breakLabel={<a href="">...</a>}
                    breakClassName={"break-me"}
                    pageCount={this.props.blogData.length / 3}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={1}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}
                />
            </div>
        )
    }
}

BlogList.propTypes = {
    blogData: PropTypes.array
}

export default BlogList