import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

import Popular from './side-screen-comps/popular'
import Archive from './side-screen-comps/archive'

class SideScreen extends Component{

    render = () => {
        return (
            <div className='side-screen'>
                <Link className='side-contact' to='/contact'>
                    Contact us today for more information. <span>&#9658;</span>
                </Link>
                <Popular randomData={this.props.blogData} />
                <Archive 
                    setGlobalFilter={this.props.setGlobalFilter}
                    archiveData={this.props.blogData}
                />
            </div>
        )
    }
}

SideScreen.propTypes = {
    blogData: PropTypes.array,
    setGlobalFilter: PropTypes.func
}

export default SideScreen