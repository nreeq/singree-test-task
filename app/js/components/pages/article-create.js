import React, {Component} from 'react'

import ArticleCreator from './../components-factory/blog-components/article-creator'
import SectionScreen from './../components-factory/screen/section-screen'

class ArticleCreate extends Component{

    render = () => {
        return (
            <div>
                <SectionScreen section='create'/>
                <div className='section article-editor'>
                    <ArticleCreator
                        asyncRequestHelper={this.props.asyncRequestHelper}
                    />
                </div>
            </div>
        )
    }
}

export default ArticleCreate