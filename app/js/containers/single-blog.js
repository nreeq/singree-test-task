import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'

import SingleBlogPage from './../components/pages/single-blog'

import {asyncRequestHelper} from './../actions/async-request-helper'

const mapStateToProps = (state, ownProps) => {

    const singleBlogData = {}

    state.blogData.forEach(item => {
        if(ownProps.match.params.id === item._id) Object.assign(singleBlogData, item)
    })

    return { 
        blogData: state.blogData,
        singleBlogData
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators (
        {
            asyncRequestHelper
        },
        dispatch
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleBlogPage);