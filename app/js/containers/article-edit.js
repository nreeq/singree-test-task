import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'

import ArticleEdit from './../components/pages/article-edit'
import {asyncRequestHelper} from './../actions/async-request-helper'

const mapStateToProps = (state, ownProps) => {

    const singleBlogData = {}

    state.blogData.forEach(item => {
        if(ownProps.match.params.id === item._id) Object.assign(singleBlogData, item)
    })

    return { singleBlogData }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators (
        { asyncRequestHelper },
        dispatch
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleEdit);