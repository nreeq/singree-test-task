import React, {Component} from 'react'

import SectionScreen from './../components-factory/screen/section-screen'
import SideScreen from './../components-factory/screen/side-screen'
import BlogList from './../components-factory/blog-components/blog-list'

import requests from './../../constants/requests'

class Blog extends Component{

    componentWillMount = () => {
        if(this.props.blogData.length < 1) {
            this.props.asyncRequestHelper(requests.getBlogData.actions, requests.getBlogData.config)
        }
    }

    render = () => {
        return (
            <div>
                <SectionScreen section='blog' />
                <div className='section blog'>
                    <BlogList blogData={this.props.blogData} />
                    <SideScreen blogData={this.props.blogData} />
                </div>
            </div>
        )
    }
}

export default Blog