# React-redux app

## Getting started

### Prerequisite

- Install [Node.js](https://nodejs.org/)

#### Instuction

##### Install modules

to install all modules:

```
$ cd nreeq-singree-test-task(hash)
$ npm install
```

##### Run development server

to run development server:

```
$ cd nreeq-singree-test-task(hash)
$ npm run dev
```

dev server will run by default at "http://localhost:9000"

##### Run production build

to run production:

```
$ cd nreeq-singree-test-task(hash)
$ npm run build
$ npm run start
```

prod server will run by default at "http://localhost:3000"

#### Clean build folder

to clean build folder:

```
$ cd nreeq-singree-test-task(hash)
$ npm run clean
```

rimraf will clean /build folder if it exists