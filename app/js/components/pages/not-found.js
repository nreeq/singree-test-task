import React, {Component} from 'react'

import SectionScreen from './../components-factory/screen/section-screen'

class NotFound extends Component{
    render = () => {
        return (
            <div>
                <SectionScreen section='ops...' />
                <div className='section resources'>
                    <div>
                        page not found
                    </div>
                </div>
            </div>
        )
    }
}

export default NotFound