import React, {Component} from 'react'

import SectionScreen from './../components-factory/screen/section-screen'

class Services extends Component{
    render = () => {
        return (
            <div>
                <SectionScreen section='services' />
                <div className='section services'>
                    <div>
                        our services
                    </div>
                </div>
            </div>
        )
    }
}

export default Services