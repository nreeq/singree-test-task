import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Editor } from '@tinymce/tinymce-react'
import {Link} from 'react-router-dom'

import requests from './../../../constants/requests'

class ArticleEditor extends Component{
    constructor(props){
        super(props)
        this.state = {
            title: '',
            body: '',
            hasProps: false
        }
    }

    componentWillReceiveProps = (nextProps) => {
        if(this.props.singleBlogData != nextProps.singleBlogData){
            this.setState({
                title: nextProps.singleBlogData.title,
                body: nextProps.singleBlogData.body,
                hasProps: true
            })
        }
    }

    componentWillMount = () => {
        this.setState({
            title: this.props.singleBlogData.title,
            body: this.props.singleBlogData.body,
        })
    }

    componentDidMount = () => {
        if(this.state.body){
            this.setState({
                hasProps: true
            })
        }
    }

    handleType = ev => this.setState({ title: ev.target.value })

    handleEditorChange = ev => this.setState({ body: ev.target.getContent() })

    saveArticle = () => {
        const updatedArticle = {
            title: this.state.title,
            body: this.state.body
        }
        this.props.asyncRequestHelper(requests.editArticle.actions, requests.editArticle.config, updatedArticle, this.props.singleBlogData._id)
    }

    renderEditor = (hasProps) => {
        if(hasProps){
            return (
                <div className='editor-content'>
                    <div className='editor-nav'>
                        <button 
                            disabled={this.state.body && this.state.body !== this.props.singleBlogData.body ||
                                    this.state.title && this.state.title !== this.props.singleBlogData.title 
                                   ? false : true}
                            onClick={this.saveArticle}
                        >save
                        </button>
                        <Link to={`/blog/${this.props.singleBlogData._id}`}>cancel / back</Link>
                    </div>
                    <input
                        value={this.state.title}
                        type='text'
                        onChange={this.handleType}
                        placeholder='Title'
                        required
                    />
                    <div className='tiny-editor'>
                    <Editor
                            value={this.state.body}
                            init={{
                                plugins: 'link image code',
                                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code | fontsizeselect',
                                fontsize_formats: '8px 10px 12px 14px 18px 24px'
                            }}
                            onChange={this.handleEditorChange}
                        />
                    </div>
                </div>
            )
        } else {
            return <h2>editor is loading... wait a second</h2>
        }
    }

    render = () => this.renderEditor(this.state.hasProps)
}

ArticleEditor.propTypes = {
    singleBlogData: PropTypes.object,
    asyncRequestHelper: PropTypes.func
}

export default ArticleEditor