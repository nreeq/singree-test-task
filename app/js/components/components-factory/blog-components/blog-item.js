import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

import BlogDate from './blog-date'

class BlogItem extends Component{
        render = () => {

        return (
            <div className='blog-item'>
                <BlogDate pubDate={this.props.pubDate} />
                <div className='blog-item-image'>
                    <img src={this.props.image}  alt={this.props.title}/>
                </div>
                <div className='blog-item-text'>
                    <h2>{this.props.title}</h2>
                    <p>{this.props.description}</p>
                    <Link to={`/blog/${this.props.link}`}>continue reading</Link>
                </div>
            </div>
        )
    }
}

BlogItem.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    link: PropTypes.string,
    pubDate: PropTypes.string,
    image: PropTypes.string
}

export default BlogItem