const express = require('express')
const app = express()
const PORT = process.env.PORT || 3000

//run server
app.listen(PORT, () => {
	console.log(`[!] App is listening on ${PORT}`)
});

app.use(express.static(__dirname + '/build'))

app.get('*', (req, res, next) => {
    res.sendFile(__dirname + '/build/index.html')
});