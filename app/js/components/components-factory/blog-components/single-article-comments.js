import React, {Component} from 'react'
import PropTypes from 'prop-types'
import crypto from 'crypto'

import SingleComment from './single-comment'
import CommentForm from './single-article-comment-form'

import requests from './../../../constants/requests'

class CommentsField extends Component{

    constructor(props){
        super(props)
        this.state={
            authorsName: '',
            authorsEmail: '',
            authorsComment: ''
        }
    }

    handleTyping = (input, value) => {
        switch(input){
            case 'author':
                this.setState({authorsName: value})
                break
            case 'email':
                this.setState({authorsEmail: value})
                break
            case 'comment':
                this.setState({authorsComment: value})
                break
        }
    }

    submitComment = () => {
        let userIndent = ''

        if(localStorage.getItem('user_def')){
            userIndent = localStorage.getItem('user_def')
        } else {
            userIndent = crypto.randomBytes(12).toString('hex')
            localStorage.setItem('user_def', userIndent)
        }
        
        const CommentData = {
            articleId: this.props.id,
            text: this.state.authorsComment,
            author: userIndent,
            authorName: this.state.authorsName
        }

        this.props.asyncRequestHelper(requests.postComment.actions, requests.postComment.config, CommentData)

        this.setState({
            authorsComment: ''
        })
    }

    mapComments = (comments) => {
        if(comments.length > 0){
            return comments.map( item => {
                return <SingleComment 
                            key={item._id}
                            authorName={item.authorName}
                            text={item.text}
                            created={item.created}
                        />
            })
        } else {
            return <li className='zero-comments'>
                        there are no comments here, why dont u make one?
                    </li>
        }
    }
    
    render = () => {
        return (
            <div className='comments-field'>
                <ul className='comments-show'>
                    {this.mapComments(this.props.comments)}
                </ul>
                <CommentForm
                    author={this.state.authorsName}
                    email={this.state.authorsEmail}
                    comment={this.state.authorsComment}
                    handleTyping={this.handleTyping}
                    submitComment={this.submitComment}
                />
            </div>
        )
    }
}

CommentsField.propTypes = {
    comments: PropTypes.array,
    id: PropTypes.string
}

export default CommentsField