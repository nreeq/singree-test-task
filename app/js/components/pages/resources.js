import React, {Component} from 'react'

import SectionScreen from './../components-factory/screen/section-screen'

class Resources extends Component{
    render = () => {
        return (
            <div>
                <SectionScreen section='resources' />
                <div className='section resources'>
                    <div>
                        rich resources
                    </div>
                </div>
            </div>
        )
    }
}

export default Resources