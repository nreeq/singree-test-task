import React, {Component} from 'react'
import {connect} from 'react-redux'

import SingleArticle from './../components-factory/blog-components/single-article'
import SectionScreen from './../components-factory/screen/section-screen'
import SideScreen from './../components-factory/screen/side-screen'

import requests from './../../constants/requests'

class SingleBlogPage extends Component {

    componentWillMount = () => {
        if(this.props.blogData.length < 1) {
            this.props.asyncRequestHelper(requests.getBlogData.actions, requests.getBlogData.config)
        }
    }

    render = () => {
        return (
            <div className='single-blog-page'>
                <SectionScreen section='blog' />
                <div className='section blog'>
                    <SingleArticle 
                        singleBlogData={this.props.singleBlogData}
                        asyncRequestHelper={this.props.asyncRequestHelper}
                    />
                    <SideScreen blogData={this.props.blogData} />
                </div>
            </div>
        )
    }
}

export default SingleBlogPage