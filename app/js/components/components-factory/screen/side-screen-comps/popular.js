import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Link} from 'react-router-dom'

class Popular extends Component{

    shuffleProjects = array => {
        const arrayCopy = array.slice()

        for (let i = arrayCopy.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [arrayCopy[i], arrayCopy[j]] = [arrayCopy[j], arrayCopy[i]];
        }
        return arrayCopy;
    }


    mapRandomArticle = articles => {
        const shuffledStaff = this.shuffleProjects(articles)

        return shuffledStaff.map((article, i) => {
            if( i > 1 ) return false
            const description = article.metaDescription && article.metaDescription.length > 54 ? `${article.metaDescription.slice(0, 54)}...` : article.metaDescription
            return (
                <Link 
                    key={i} 
                    to={`/blog/${article._id}`} 
                    className='single-random-article'
                >
                    <h5>{article.title}</h5>
                    <p>{description}</p>
                </Link>
            )
        })
    }

    render = () => {
        return (
            <div className='side-popular'>
                <div className='header'>
                    <span>&#9733;</span> popular
                </div>
                <div className='side-random-article-list'>
                    {this.mapRandomArticle(this.props.randomData)}
                </div>
            </div>
        )
    }
}

Popular.propTypes = {
    randomData: PropTypes.array
}

export default Popular