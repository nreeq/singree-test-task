import React, {Component} from 'react'
import PropTypes from 'prop-types'
import FontAwesome from 'react-fontawesome'
import {Link} from 'react-router-dom'

import {getCurrentTime} from './../../../../helpers/time-parser'

class Archive extends Component{

    constructor(props){
        super(props)
        this.state = {
            filter: ''
        }
    }

    setFilter = year => {
        if(year !== this.state.filter){
            this.setState({ filter: year })
        } else {
            this.setState({ filter: '' })
        }
    }

    renderMonthItems = (filter, data) => {
        let quantity = 0
        let month = ''
        return data.map((item, i) => {
            if(filter === getCurrentTime(item.created, 'year')){
                if(month !== getCurrentTime(item.created, 'month-name')){
                    month = getCurrentTime(item.created, 'month-name')
                    return (
                        <Link
                            to='/'
                            className='archive-month' 
                            key={i}
                        >
                            {getCurrentTime(item.created, 'month-name')}
                        </Link>
                    )   
                }
            }
        })
    }

    renderArhicveList = data => {

        let getYears = [...new Set(data.map(item => getCurrentTime(item.created, 'year')))]

        return getYears.map((item, i) => {
            return (
                <div 
                    className='archive-item'
                    key={i}
                >
                    <h5
                        className='archive-year'
                        onClick={() => this.setFilter(item)}
                    >
                        <FontAwesome name='calendar' />
                        {item}
                        {this.state.filter === item ? <FontAwesome name='angle-up' /> : <FontAwesome name='angle-down' />}
                    </h5>
                    <div className='archive-list'>
                        {this.state.filter ? this.renderMonthItems(this.state.filter, data) : null}
                    </div>
                </div>
            )
        })
    }

    render = () => {
        return (
            <div className='side-archive'>
                <div className='archive-head'>
                    <FontAwesome name='archive' /> 
                    archive
                </div>
                <div className='archive-list'>
                    {this.renderArhicveList(this.props.archiveData)}
                </div>
            </div>
        )
    }
}

Archive.propTypes = {
    archiveData: PropTypes.array
}

export default Archive