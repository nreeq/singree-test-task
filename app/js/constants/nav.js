const nav = [
    {
        id: '01',
        value: 'about us',
        link: '/about'
    },
    {
        id: '02',
        value: 'services',
        link: '/services'
    },
    {
        id: '03',
        value: 'guarantee',
        link: '/guarantee'
    },
    {
        id: '04',
        value: 'resources',
        link: '/resources'
    },
    {
        id: '05',
        value: 'forms',
        link: '/forms'
    },
    {
        id: '06',
        value: 'contact',
        link: '/contact'
    }
]

export default nav