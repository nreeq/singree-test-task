import React, {Component} from 'react'
import PropTypes from 'prop-types'

class SingleComment extends Component{
    render = () => {
        return( 
            <li className='comment'>
                <div className='head'>
                    <p className='author'>
                        {this.props.authorName}
                    </p>
                    <p className='publish-date'>
                        {this.props.created}
                    </p>
                </div>
                <p className='message'>
                    {this.props.text}
                </p>
            </li>
        )
    }
}

SingleComment.propTypes = {
    authorName: PropTypes.string,
    text: PropTypes.string,
    created: PropTypes.string
}

export default SingleComment