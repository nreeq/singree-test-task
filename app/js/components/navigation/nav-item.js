import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {NavLink} from 'react-router-dom'

class NavItem extends Component{
    render(){
        return (
            <NavLink 
                activeClassName="nav-item-active"
                className='nav-item' 
                to={this.props.link}
            >
                {this.props.value}
            </NavLink>
        )
    }
}

NavItem.propTypes = {
    link: PropTypes.string,
    value: PropTypes.string
}

export default NavItem