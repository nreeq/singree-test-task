import React, {Component} from 'react'

import SectionScreen from './../components-factory/screen/section-screen'

class Forms extends Component{
    render = () => {
        return (
            <div>
                <SectionScreen section='forms' />
                <div className='section form'>
                    <div>
                        some forms
                    </div>
                </div>
            </div>
        )
    }
}

export default Forms