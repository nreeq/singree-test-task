const blogReducer = (state = [], action) => {
    switch (action.type) {
        case "BLOG_FETCH_REQUEST":
            return state
        case "BLOG_FETCH_SUCCESS": 
            return action.payload.articles
        case "BLOG_FETCH_ERROR":
            console.log('could not fetch some data :(')
            return state
        case "COMMENT_POST_REQUEST":
            return state
        case "COMMENT_POST_SUCCESS":
            let updatedArticles = state.slice()
            updatedArticles.forEach(item => {
                if(item._id === action.payload.articleId){
                    item.comments.push(action.payload)
                }
            })
            return updatedArticles
        case "COMMENT_POST_ERROR":
            console.log('could not post comment')
            return state
        case "ARTICLE_EDIT_REQUEST":
            return state
        case "ARTICLE_EDIT_SUCCESS": 
            let createdArticle = state.slice()
            createdArticle.forEach(item => {
                if(item._id === action.payload._id){
                    item.body = action.payload.body,
                    item.title = action.payload.title
                }
            })
            return createdArticle
        case "ARTICLE_EDIT_ERROR":
            console.log('cound not updated article')
            return state
        case "ARTICLE_ADD_REQUEST":
            return state
        case "ARTICLE_ADD_SUCCESS": 
            let editedArticle = [action.payload, ...state]
            return editedArticle
        case "ARTICLE_ADD_ERROR":
            console.log('cound not updated article')
            return state
        case "GLOBAL_FILTER_IS_SET":
            console.log(action.payload)
            return state
        default:
            return state
      }
}

export {blogReducer}