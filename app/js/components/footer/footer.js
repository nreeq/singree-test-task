import React, {Component} from 'react'
import {Link} from 'react-router-dom'

class Footer extends Component{
    render = () => {
        return (
            <div className='footer'>
                <p>© Windsor Publishing Inc. 2016</p>
                <Link to='/contact'>Contact us</Link> 
            </div>
        )
    }
}

export default Footer