const requests = {
    addArticle: {
        actions: {
            request: 'ARTICLE_ADD_REQUEST',
            success: 'ARTICLE_ADD_SUCCESS',
            error: 'ARTICLE_ADD_ERROR'
        },
        config: {
            route: 'http://api.blog.testing.singree.com/',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        }
    },
    editArticle: {
        actions: {
            request: 'ARTICLE_EDIT_REQUEST',
            success: 'ARTICLE_EDIT_SUCCESS',
            error: 'ARTICLE_EDIT_ERROR'
        },
        config: {
            route: 'http://api.blog.testing.singree.com/',
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            }
        }
    },
    getBlogData: {
        actions: {
            request: 'BLOG_FETCH_REQUEST',
            success: 'BLOG_FETCH_SUCCESS',
            error: 'BLOG_FETCH_ERROR'
        },
        config: {
            route: 'http://api.blog.testing.singree.com/',
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }
    },
    postComment: {
        actions: {
            request: 'COMMENT_POST_REQUEST',
            success: 'COMMENT_POST_SUCCESS',
            error: 'COMMENT_POST_ERROR'
        },
        config: {
            route: 'http://api.blog.testing.singree.com/comment/',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        }
    }
}

export default requests