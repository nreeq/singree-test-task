import '../scss/main.scss'
import {BrowserRouter as Router, Route, Switch, browserHistory} from 'react-router-dom'
import ReactDOM, {render} from 'react-dom'
import React from 'react'
import {createStore, applyMiddleware} from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'

import mainReducer from './reducers'

//comps
import Navigation from './components/navigation/nav-menu'
import Footer from './components/footer/footer'

//connected
import Blog from './containers/blog'
import SingleBlogPage from './containers/single-blog'
import ArticleEdit from './containers/article-edit'
import ArticleCreate from './containers/article-create'

//disconnected
import AboutUs from './components/pages/about-us'
import Contact from './components/pages/contact'
import Forms from './components/pages/forms'
import Guarantee from './components/pages/guarantee'
import Resources from './components/pages/resources'
import Services from './components/pages/services'
import NotFound from './components/pages/not-found'

const store = createStore(mainReducer, composeWithDevTools(applyMiddleware(thunk)))

const App = () => {
    return (
        <Provider store={store}>
            <Router history={browserHistory}>
                    <div className='app'>
                        <Navigation />
                        <Switch>
                            <Route exact path='/' component={Blog} />
                            <Route exact path='/about' component={AboutUs} />
                            <Route exact path='/services' component={Services} />
                            <Route exact path='/guarantee' component={Guarantee} />
                            <Route exact path='/resources' component={Resources} />
                            <Route exact path='/forms' component={Forms} />
                            <Route exact path='/contact' component={Contact} />
                            <Route exact path='/blog/create' component={ArticleCreate} />
                            <Route exact path='/blog/:id' component={SingleBlogPage} />
                            <Route exact path='/blog/:id/edit' component={ArticleEdit} />
                            <Route path='*' component={NotFound} />
                        </Switch>
                        <Footer />
                    </div>
            </Router>
        </Provider>
    )
}

render(<App />, document.getElementById('root'))