import React, {Component} from 'react'
import PropTypes from 'prop-types'
import FontAwesome from 'react-fontawesome'

class ShareSection extends Component{
    shareClick = (name) => {
        console.log(`share via ${name}`)
    }

    render = () => {
        return (
            <div className='single-article-share'>
                <div onClick={() => this.shareClick('MAIN-SHARE')} className='share'>
                    <FontAwesome className='main-share-btn' name='share-alt'/>
                    <label htmlFor='main-share-btn'>share</label>
                </div>
                <div className='social'>
                    <FontAwesome onClick={() => this.shareClick('FACEBOOK')} name='facebook-square'/>
                    <FontAwesome onClick={() => this.shareClick('GOOGLE-PLUS')} name='google-plus-square'/>
                    <FontAwesome onClick={() => this.shareClick('TWITTER')} name='twitter-square'/>
                    <FontAwesome onClick={() => this.shareClick('LINKEDIN')} name='linkedin-square'/>
                    <FontAwesome onClick={() => this.shareClick('TUMBLR')} name='tumblr-square'/>
                </div>
            </div>
        )
    }
}

ShareSection.propTypes = {}

export default ShareSection