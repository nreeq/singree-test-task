import {bindActionCreators} from 'redux'
import { connect } from 'react-redux'

import Blog from './../components/pages/blog'
import {asyncRequestHelper} from './../actions/async-request-helper'

const mapStateToProps = state => {
	return {
		blogData: state.blogData
	}
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators (
        {
            asyncRequestHelper
        },
        dispatch
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(Blog);